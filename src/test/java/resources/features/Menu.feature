@Menu
Feature: Monitoreo de Opciones de menú en Producción para aplicativo MTO GDE 

Background: 1.0 - Validar el correcto ingreso a web OMT
	Given Accedo a Web OMTGDE
	And Ingreso Usuario CL Valido
	And Ingreso Contraseña CL correcta
	And Selecciono Cadena Ripley CL
	When Realizo click en Botón Entrar
	And Selecciono sucursal de Trabajo Redex
	Then Se despliega en pantalla web MTO con mensaje de bienvenida

@Caso1.1
Scenario: Caso 1.1 - Desplegar opciones de menú seleccionando Opción Emisión Guías Unitarias
	Given Despliego las opciones de menu
	When Selecciono Emisión Guías
	Then Selecciono Opción Emisión Guías Unitarias
	And Se despliega Pantalla Emisión Guías Unitarias

@Caso2.1
Scenario: Caso 2.1 - Desplegar opciones de menú seleccionando Opción Emisión Masiva
	Given Despliego las opciones de menu
	When Selecciono Emisión Guías
	Then Selecciono Opción Emisión Masiva
	And Se despliega Pantalla Emisión Masiva

@Caso3.1
Scenario: Caso 3.1 - Desplegar opciones de menú seleccionando Reimpresión de Guías
	Given Despliego las opciones de menu
	When Selecciono Emisión Guías
	Then Selecciono Opción Reimpresión de Guías
	And Se despliega Pantalla Reimpresión de Guías
	
@Caso4.1
Scenario: Caso 4.1 - Desplegar opciones de menú seleccionando Opción Reporte Reimpresión
	Given Despliego las opciones de menu
	When Selecciono Emisión Guías
	Then Selecciono Opción Reporte Reimpresión
	And Se despliega Pantalla Reporte Reimpresión

@Caso5.1
Scenario: Caso 5.1 - Desplegar opciones de menú seleccionando Opción Guías Transferencia
	Given Despliego las opciones de menu
	When Selecciono Emisión Guías
	Then Selecciono Opción Guías Transferencia
	And Se despliega Pantalla Guías Transferencia
	
@Caso6.1
Scenario: Caso 6.1 - Desplegar opciones de menú seleccionando Opción Reporte Guías Emitidas
	Given Despliego las opciones de menu
	When Selecciono Emisión Guías
	Then Selecciono Opción Reporte Guías Emitidas
	And Se despliega Pantalla Reporte Guías Emitidas
	
@Caso7.1
Scenario: Caso 7.1 - Desplegar opciones de menú seleccionando Opción Guía Flujo DVO
	Given Despliego las opciones de menu
	When Selecciono Emisión Guías
	Then Selecciono Opción Reporte Guía Flujo DVO
	And Se despliega Pantalla Reporte Guía Flujo DVO
	
	
	
	