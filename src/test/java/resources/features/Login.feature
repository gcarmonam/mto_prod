@Login
Feature: Realizar el correcto ingreso a Emisión de Guías

Scenario: 1.0 - Validar el correcto ingreso a web OMT
	Given Accedo a Web OMTGDE
	And Ingreso Usuario CL Valido
	And Ingreso Contraseña CL correcta
	And Selecciono Cadena Ripley CL
	When Realizo click en Botón Entrar
	And Selecciono sucursal de Trabajo Redex
	Then Se despliega en pantalla web MTO con mensaje de bienvenida