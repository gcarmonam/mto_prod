package utils;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import pageObjects.LoginPage;
import pageObjects.MenuPage;

public class DriverFactory {
	public static WebDriver driver;
	public static LoginPage loginPage;
	public static MenuPage menuPage;
	
	@SuppressWarnings("deprecation")
	public WebDriver getDriver() {
		try {
			ReadConfigFile file = new ReadConfigFile();
			String browserName = file.getBrowser();

			switch (browserName) {

			case "firefox":
				if (null == driver) {
					System.setProperty("webdriver.gecko.driver", Constant.GECKO_DRIVER_DIRECTORY);
					DesiredCapabilities capabilities = DesiredCapabilities.firefox();
					capabilities.setCapability("marionette", true);
					driver = new FirefoxDriver();
				}
				break;

			case "chrome":
				if (null == driver) {
					System.setProperty("webdriver.chrome.driver", Constant.CHROME_DRIVER_DIRECTORY);
					driver = new ChromeDriver();
					driver.manage().window().maximize();					
				}
				break;

			case "ie":
				if (null == driver) {
					DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
					System.setProperty("webdriver.ie.driver", Constant.IE_DRIVER_DIRECTORY);
					capabilities.setCapability("ignoreZoomSetting", true);
					driver = new InternetExplorerDriver(capabilities);
					driver.manage().window().maximize();
				}
				break;
			}
		} catch (Exception e) {
			System.out.println("No se cargo browser: " + e.getMessage());
		} finally {
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			loginPage				= PageFactory.initElements(driver, LoginPage.class);
			menuPage				= PageFactory.initElements(driver, MenuPage.class);
		}
		return driver;
	}

	public static String getUserCL() {
		ReadConfigFile file = new ReadConfigFile();
		String userCL = file.getUserCL();
		return userCL;
	}
	
	public static String getPassCL() {
		ReadConfigFile file = new ReadConfigFile();
		String passCL = file.getPassCL();
		return passCL;
	}
}
