package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.BasePage;
import utils.DriverFactory;

public class MenuSteps extends DriverFactory {
	
	@Given("^Despliego las opciones de menu$")
	public void despliego_las_opciones_de_menu() throws Throwable {
	    menuPage.menuPrincipal();
	    BasePage.waitSleep(2);
	}

	@When("^Selecciono Emisión Guías$")
	public void selecciono_Emisión_Guías() throws Throwable {
	    menuPage.menuEmision();
	    BasePage.waitSleep(2);
	}

	@Then("^Selecciono Opción Emisión Guías Unitarias$")
	public void selecciono_Opción_Emisión_Guías_Unitarias() throws Throwable {
	    menuPage.menuUnitaria();
	    BasePage.waitSleep(2);
	}

	@Then("^Se despliega Pantalla Emisión Guías Unitarias$")
	public void se_despliega_Pantalla_Emisión_Guías_Unitarias() throws Throwable {
	    BasePage.takeScreenShot();
	}

	@Then("^Selecciono Opción Emisión Masiva$")
	public void selecciono_Opción_Emisión_Masiva() throws Throwable {
	    menuPage.menuMasiva();
	    BasePage.waitSleep(2);
	}

	@Then("^Se despliega Pantalla Emisión Masiva$")
	public void se_despliega_Pantalla_Emisión_Masiva() throws Throwable {
	    BasePage.takeScreenShot();
	}
	
	@Then("^Selecciono Opción Reimpresión de Guías$")
	public void selecciono_Opción_Reimpresión_de_Guías() throws Throwable {
	    menuPage.menuReimpresion();
	    BasePage.waitSleep(2);
	}

	@Then("^Se despliega Pantalla Reimpresión de Guías$")
	public void se_despliega_Pantalla_Reimpresión_de_Guías() throws Throwable {
	    BasePage.takeScreenShot();
	}

	@Then("^Selecciono Opción Reporte Reimpresión$")
	public void selecciono_Opción_Reporte_Reimpresión() throws Throwable {
	    menuPage.menuReporte();
	    BasePage.waitSleep(2);
	}

	@Then("^Se despliega Pantalla Reporte Reimpresión$")
	public void se_despliega_Pantalla_Reporte_Reimpresión() throws Throwable {
		BasePage.takeScreenShot();
	}

	@Then("^Selecciono Opción Guías Transferencia$")
	public void selecciono_Opción_Guías_Transferencia() throws Throwable {
	    menuPage.menuTransferencia();
	    BasePage.waitSleep(2);
	}

	@Then("^Se despliega Pantalla Guías Transferencia$")
	public void se_despliega_Pantalla_Guías_Transferencia() throws Throwable {
		BasePage.takeScreenShot();
	}

	@Then("^Selecciono Opción Reporte Guías Emitidas$")
	public void selecciono_Opción_Reporte_Guías_Emitidas() throws Throwable {
	    menuPage.menuEmitidas();
	    BasePage.waitSleep(2);
	}

	@Then("^Se despliega Pantalla Reporte Guías Emitidas$")
	public void se_despliega_Pantalla_Reporte_Guías_Emitidas() throws Throwable {
		BasePage.takeScreenShot();
	}

	@Then("^Selecciono Opción Reporte Guía Flujo DVO$")
	public void selecciono_Opción_Reporte_Guía_Flujo_DVO() throws Throwable {
	   menuPage.menuDVO();
	   BasePage.waitSleep(2);
	}

	@Then("^Se despliega Pantalla Reporte Guía Flujo DVO$")
	public void se_despliega_Pantalla_Reporte_Guía_Flujo_DVO() throws Throwable {
		BasePage.takeScreenShot();
	}
}
