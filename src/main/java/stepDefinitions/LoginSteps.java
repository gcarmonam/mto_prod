package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.BasePage;
import utils.DriverFactory;

public class LoginSteps extends DriverFactory{
	
	@Given("^Accedo a Web OMTGDE$")
	public void accedo_a_Web_OMTGDE() throws Throwable {
	    loginPage.getMTOAlphaWeb();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@Given("^Ingreso Usuario CL Valido$")
	public void ingreso_Usuario_CL_Valido() throws Throwable {
		loginPage.ingresarUsuario(getUserCL());
		BasePage.waitSleep(1);
	}

	@Given("^Ingreso Contraseña CL correcta$")
	public void ingreso_Contraseña_CL_correcta() throws Throwable {
		loginPage.ingresarPassword(getPassCL());
		BasePage.waitSleep(1);
	}

	@Given("^Selecciono Cadena Ripley CL$")
	public void selecciono_Cadena_Ripley_CL() throws Throwable {
	    loginPage.selCadena();
	    BasePage.waitSleep(2);
	    BasePage.takeScreenShot();
	}

	@When("^Realizo click en Botón Entrar$")
	public void realizo_click_en_Botón_Entrar() throws Throwable {
	    loginPage.btnEntrar();
	    BasePage.waitSleep(5);
	}

	@When("^Selecciono sucursal de Trabajo Redex$")
	public void selecciono_sucursal_de_Trabajo_Redex() throws Throwable {
		loginPage.busquedaSucursal("Redex");
	    BasePage.waitSleep(3);
	}

	@Then("^Se despliega en pantalla web MTO con mensaje de bienvenida$")
	public void se_despliega_en_pantalla_web_MTO_con_mensaje_de_bienvenida() throws Throwable {
		BasePage.takeScreenShot();
	}
}
