package pageObjects;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {
	
	public @FindBy(id = "usuario") WebElement txt_Usuario;
	public @FindBy(id = "password") WebElement txt_Password;
	public @FindBy(id = "boton") WebElement btn_Entrar;
	public @FindBy(id = "branchOfficeSelected") WebElement txt_Sucursal;
	public @FindBy(xpath = "/html/body/app-root/app-login/section/div/div/div/div/form/div[3]/div/div/select/option[2]") WebElement sel_Cadena;

	public LoginPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public LoginPage getMTOAlphaWeb() throws IOException {
		getDriver().get("https://mtoweb.ripleyprd.com/MTDWeb/index.html#/login");
	return new LoginPage();
	}
	
	public LoginPage ingresarUsuario(String firstName) throws Exception {
		sendKeysToWebElement(txt_Usuario, firstName);
	return new LoginPage();
	}
	
	public LoginPage ingresarPassword(String password) throws Exception {
		sendKeysToWebElement(txt_Password, password);
	return new LoginPage();
	}
	
	public LoginPage selCadena() throws IOException {
		sel_Cadena.click();
	return new LoginPage();
	}
	
	public LoginPage btnEntrar() throws IOException {
		btn_Entrar.click();
	return new LoginPage();
	}
	
	public LoginPage busquedaSucursal(String nroSucursal) throws Exception {
		clickOnTextFromDropdownList(txt_Sucursal,nroSucursal);
		return new LoginPage();
	}
	

}
