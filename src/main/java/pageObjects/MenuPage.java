package pageObjects;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MenuPage extends BasePage{
	
	public @FindBy(xpath = "/html/body/app-root/app-back-office/app-menu/div/div[2]/div[2]") WebElement menu_Principal;
	public @FindBy(xpath = "/html/body/app-root/app-back-office/app-menu/div/div[2]/ul/li[2]") WebElement menu_Emision;
	public @FindBy(xpath = "/html/body/app-root/app-back-office/app-menu/div/div[2]/ul/li[2]/ul/li[1]") WebElement menu_Unitaria;
	public @FindBy(xpath = "/html/body/app-root/app-back-office/app-menu/div/div[2]/ul/li[2]/ul/li[2]") WebElement menu_Masiva;
	public @FindBy(xpath = "/html/body/app-root/app-back-office/app-menu/div/div[2]/ul/li[2]/ul/li[3]") WebElement menu_Reimpresion;
	public @FindBy(xpath = "/html/body/app-root/app-back-office/app-menu/div/div[2]/ul/li[2]/ul/li[4]") WebElement menu_Reporte;
	public @FindBy(xpath = "/html/body/app-root/app-back-office/app-menu/div/div[2]/ul/li[2]/ul/li[5]") WebElement menu_Transferencia;
	public @FindBy(xpath = "/html/body/app-root/app-back-office/app-menu/div/div[2]/ul/li[2]/ul/li[6]") WebElement menu_Emitidas;
	public @FindBy(xpath = "/html/body/app-root/app-back-office/app-menu/div/div[2]/ul/li[2]/ul/li[7]") WebElement menu_DVO;
 
	public MenuPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public MenuPage menuPrincipal() throws IOException{
		menu_Principal.click();
	return new MenuPage();
	}
	
	public MenuPage menuEmision() throws IOException{
		menu_Emision.click();
	return new MenuPage();
	}
	
	public MenuPage menuUnitaria() throws IOException{
		menu_Unitaria.click();
	return new MenuPage();
	}
	
	public MenuPage menuMasiva() throws IOException{
		menu_Masiva.click();
	return new MenuPage();
	}
	
	public MenuPage menuReimpresion() throws IOException{
		menu_Reimpresion.click();
	return new MenuPage();
	}
	
	public MenuPage menuReporte() throws IOException{
		menu_Reporte.click();
	return new MenuPage();
	}
	
	public MenuPage menuTransferencia() throws IOException{
		menu_Transferencia.click();
	return new MenuPage();
	}
	
	public MenuPage menuEmitidas() throws IOException{
		menu_Emitidas.click();
	return new MenuPage();
	}
	
	public MenuPage menuDVO() throws IOException{
		menu_DVO.click();
	return new MenuPage();
	}

}
